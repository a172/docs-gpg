# Native

## Install stuff

### OpenSSH

See the Microsoft [instructions][3] to install the OpenSSH client.
The server is not needed.

Verify openssh is installed:
```
C:\Users\a172>ssh -V
OpenSSH_for_Windows_8.1p1, LibreSSL 3.0.2
```

### winget

Installing the `winget` tool will make things go much easier.

- Open the Microsoft Store
- Search for "[App Installer][7]" (by Microsoft Corporation)
- Install the application

Verify `winget` is installed:
```
C:\Users\example>winget -v
v1.2.10271
```

### GnuPG

Install GnuPG with:
```
C:\Users\example>winget install gnupg
```

Verify it is installed with:
```
C:\Users\example>gpg --version
```

### git

Install git with:
```
C:\Users\example>winget install git.git
```

To verify it is installed, open PowerShell and run:
```
PS C:\Users\example>git --version
git version 2.36.1.windows.1
```

### Install QtPass

```
C:\Users\a172>winget install qtpass
```

If you get and "Access is denied." error message, try again from an
administrator terminal.
This is a bug in the qtpass installer.


## Setup

### Generate a PGP key set

- Open a terminal
- Run `gpg --quick-generate-key your_pid@vt.edu`
- When prompted, enter a strong, unique passphrase.

### Generate an SSH key

- Open a terminal
- Run `ssh-keygen -t ed25519`
- When prompted, enter a strong, unique passphrase.
  Note this is distinct from the passphrase on the PGP key.

### Add SSH key to code.vt.edu

- Browse to [code.vt.edu][4]
- Click the user icon in the top left
- Click either "Edit profile" or "Preferences"
- In the navbar on the left, click "[SSH Keys][5]"
- Copy the contents of `C:\Users\<youruser>\.ssh\id_ed25519.pub` into the "Key"
  text box.
- Click "Add key"
- Verify the was added correctly by running `ssh git@code.vt.edu`.
  - Enter your SSH passphrase when prompted
  - You should see an output similar to the following:
    ```
    Enter passphrase for key 'C:\Users\example/.ssh/id_ed25519':
    PTY allocation request failed on channel 0
    Welcome to GitLab, @waldrep!
    Connection to code.vt.edu closed.
    ```

### Clone password repository

Open PowerShell:
```
PS C:\Users\example> git config --global user.email "example@vt.edu"
PS C:\Users\example> git config --global user.name "Exam Poll"
PS C:\Users\example> git clone git@code.vt.edu:dit-nis-network/passwords.git pasword-store
Cloning into 'password-store'...
Enter passphrase for key '/c/Users/a172/.ssh/id_ed25519':
remote: Enumerating objects: 1873, done.
remote: Counting objects: 100% (346/346), done.
remote: Compressing objects: 100% (257/257), done.
remote: Total 1873 (delta 78), reused 303 (delta 58), pack-resused 1527
Receiving objects: 100%0(1873/1873), 3.02 MiB | 19.80 MiB/s, done.
Resolving detlas: 100% (310/310), done.
PS C:\Users\example>
```

### Import keys

```
PS C:\Users\example> cd password-store
PS C:\Users\a172\password-store> gpg --import .keys/*
```

### Establish trust

TODO: fill this out. Looks like we need to to tsigns.

### Add your info to the password store

Export your key:
```
PS C:\Users\example\password-store> gpg -a --export 0x1a2717e74b12127 > .keys/0x1a2717e74b12127.asc
```

Add your Key ID, PID, and Mobile # to the table in README.md.
Note this table is sorted by Key ID.
Please keep it this way for ease of lookups.

Add the modified files to git.
```
PS C:\Users\example\password-store> git status
...
PS C:\Users\example\password-store> git add .\README.md .\.keys
PS C:\Users\example\password-store> git commit -m 'add example key and info'
```

## Setup QtPass

- Open QtPass
- Click "OK" for the "GnuPG not found" error
- Click "OK" on the "Can not get key list" error
- Click the "X" to close the "Read access users" dialog
- In the Configuration dialog, in the Programs tab, select "Native git/gpg"
- Fill in:
  - gpg: C:/Program Files (x86)/gnupg/gin/gpg.exe
  - git: C:/Program Files/Git/bin/git.exe

[3]: https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_install_firstuse
[4]: https://code.vt.edu
[5]: https://code.vt.edu/-/profile/keys
[7]: https://apps.microsoft.com/store/detail/app-installer/9NBLGGH4NNS1?hl=en-us&gl=US
